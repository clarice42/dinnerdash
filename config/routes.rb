Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #rotas para categoria
  get "/category/index", to: "meal_categories#index"
  get "/category/show/:id", to: "meal_categories#show"
  post "/category/create", to: "meal_categories#create"
  patch "/category/update/:id", to: "meal_categories#update"
  delete "/category/destroy/:id", to: "meal_categories#destroy"
  #rotas para meals
  post "/meal/create", to: "meals#create"
  patch "/meal/update/:id", to: "meals#update"
  delete "/meal/destroy/:id", to: "meals#destroy"
  get "/meal/show/:id", to: "meals#show"
  #rotas para order_meal
  get "/order_meal/show/:id", to: "order_meals#show"
  post "/order_meal/create", to: "order_meals#create"
  patch "/order_meal/update/:id", to: "order_meals#update"
  delete "/order_meal/destroy/:id", to: "order_meals#destroy"
  #rotas para order
  get "/order/show/:id", to: "orders#show"
  post "/order/create", to: "orders#create"
  patch "/order/update/:id", to: "orders#update"
  delete "/order/delete/:id", to: "orders#destroy"

end