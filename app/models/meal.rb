class Meal < ApplicationRecord
    belongs_to :meal_category
    has_many :order_meals, dependent: :destroy
    validates :name, presence: true
    validates :description, presence: true
    validates :price, presence: true
    validates :available, presence: true
end
