class OrderMeal < ApplicationRecord
  belongs_to :meal
  belongs_to :order
  validates :quantity, presence: true
  
end
