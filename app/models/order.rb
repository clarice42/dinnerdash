class Order < ApplicationRecord
    has_many :order_meals, dependent: :destroy
    validates :price, presence: true
end
