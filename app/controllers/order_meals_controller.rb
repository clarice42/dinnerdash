class OrderMealsController < ApplicationController

    def show
        @order = OrderMeal.find(params[:id])
        render json: @order
    end

    def create
        @order = OrderMeal.create(order_params)
        if @order.save
            render json: {status: 'SUCCESS', message: 'Order meal created', data: @order}, status: :ok
        else
            render json: {status: 'ERROR', message: 'Order meal not created', data: @order.errors}, status: :unprocessable_entity
        end
    end

    def update
        @order = OrderMeal.find(params[:id])
        if @order.update(order_params)
            render json: {status: 'SUCCESS', message: 'Updated order meal', data: @order}, status: :ok
        else
            render json: {status: 'ERROR', message: 'Order meal not updated', data: @order.errors}, status: :unprocessable_entity
        end
    end

    def destroy
        @order = OrderMeal.find(params[:id])
        @order.destroy
        render json: {status: 'SUCCESS', message: 'Deleted order meal', data: @order}, status: :ok
    end

    private
        def order_params
            params.require(:order_meal).permit(:quantity, :meal_id, :order_id)
        end

end
