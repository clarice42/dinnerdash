class MealsController < ApplicationController
    
    def show
        @meals = Meal.find(params[:id])
        render json: @meals
    end

    def create
        @meals = Meal.create(meal_params)
        if @meals.save
            render json: {status: 'SUCCESS', message: 'Meal created', data: @meals}, status: :ok
        else
            render json: {status: 'ERROR', message: 'Meal not created', data: @meals.errors}, status: :unprocessable_entity
        end
    end

    def update
        @meals = Meal.find(params[:id])
        if @meals.update(meal_params)
            render json: {status: 'SUCCESS', message: 'Meal updated', data: @meals}, status: :ok
        else
            render json: {status: 'ERROR', message: 'Meal not updated', data: @meals.errors}, status: :unprocessable_entity 
        end
    end

    def destroy
        @meals = Meal.find(params[:id])
        @meals.destroy
        render json: {status: 'SUCCESS', message: 'Meal deleted', data: @meals}, status: :ok
    end

    def set_category
        @category = MealCategory.find(params[:id])
    end

    def category_meals
        render json: @category.meals, status: 200
    end

    private
        def meal_params
            params.require(:meal).permit(:name, :description, :price, :available, :meal_category_id)
        end


end
