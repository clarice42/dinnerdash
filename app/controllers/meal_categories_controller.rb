class MealCategoriesController < ApplicationController
  
    def index
        @category = MealCategory.all
        render json: @category
    end

    def show
        @category = MealCategory.find(params[:id])
        render json: @category
    end

    def create
        @category = MealCategory.create(name: params[:name])
        if @category.save
            render json: {status: 'SUCCESS', message: 'Category created', data: @category}, status: :ok
        else
            render json: {status: 'ERROR', message: 'Category not created', data: @category.errors}, status: :unprocessable_entity
        end
    end

    def update
        @category = MealCategory.find(params[:id])
        if @category.update(name: params[:name])
            render json: {status: 'SUCCESS', message: 'Category updated', data: @category}, status: :ok
        else
            render json: {status: 'ERROR', message: 'Category not updated', data: @category.errors}, status: :unprocessable_entity 
        end
    end
    
    def destroy
        @category = MealCategory.find(params[:id])
        @category.destroy
        render json: {status: 'SUCCESS', message: 'Category deleted', data: @category}, status: :ok
    end

end
