class OrdersController < ApplicationController

    def show
        @orders = Order.find(params[:id])
        render json: @orders
    end

    def create
        @orders = Order.create(price: params[:price])
        if @orders.save
            render json: {status: 'SUCCESS', message: 'Order created', data: @orders}, status: :ok
        else
            render json: {status: 'ERROR', message: 'Order not created', data: @orders.errors}, status: :unprocessable_entity
        end
    end

    def update
        @orders = Order.find(params[:id])
        if @order.update(price: params[:price])
            render json: {status: 'SUCCESS', message: 'Updated order', data: @orders}, status: :ok
        else
            render json: {status: 'ERROR', message: 'Order not updated', data: @orders.errors}, status: :unprocessable_entity
        end
    end

    def destroy
        @orders = Order.find(params[:id])
        @orders.destroy
        render json: {status: 'SUCCESS', message: 'Deleted order', data: @order}, status: :ok
    end
    
end
